/* 2.1 Convierte la siguiente promesa para esperar a ejecutar el console usando 
async-await. */
const runTimeOut = () => {
    const promise = new Promise((resolve) => {
        setTimeout(function () {
            resolve();
        }, 2000);
    })

    promise.then(() => {console.log('2.1','Then: Time out!')})
};

runTimeOut();

const runTimeOutAsync = () => {
    const promise = new Promise((resolve) => {
        setTimeout(function () {
            resolve();
        }, 2000);
    })

    const asyncfunction = async () => {
        await promise;
        console.log('2.1','Async: Time out!');
    }
    asyncfunction();
}

runTimeOutAsync();

/* 2.2 Convierte la siguiente función con un fetch utilizando async-await. 
Recuerda que para usar .fetch() tendrás que probar el ejercicio en el navegador; */
function getCharacters () {
    fetch('https://rickandmortyapi.com/api/character')
    .then(res => res.json())
    .then(characters => console.log('2.2. Then', characters));
}

getCharacters();

async function getCharactersAsync () {
    const ApiCharacters = await fetch('https://rickandmortyapi.com/api/character');
    const JsonApiCharacters = await ApiCharacters.json();
    console.log('2.2 Async', JsonApiCharacters);
}

getCharactersAsync();

