/* 1.1 Utiliza esta url de la api Agify 'https://api.agify.io?name=michael' para 
hacer un .fetch() y recibir los datos que devuelve. Imprimelo mediante un 
console.log(). Para ello, es necesario que crees un .html y un .js. */


fetch('https://api.agify.io?name=michael')
  .then((response) => {
    return response.json();
  })
  .then((myJson) => {
    console.log('1.1', myJson);
  });

/* 2.1 Dado el siguiente javascript y html. Añade la funcionalidad necesaria usando 
fetch() para hacer una consulta a la api cuando se haga click en el botón, 
pasando como parametro de la api, el valor del input.*/
const baseUrl = 'https://api.nationalize.io';

document.querySelector('button').addEventListener('click', () => {
    const name$$ = document.querySelector('input').value;

    fetch(`${baseUrl}?name=${name$$}`)
    .then((response) => {
        return response.json();
    })
    .then((json) => {
        console.log('2.1', json);
    })
});

/* 2.3 En base al ejercicio anterior. Crea dinamicamente un elemento  por cada petición 
a la api que diga...'El nombre X tiene un Y porciento de ser de Z' etc etc.
EJ: El nombre Pepe tiene un 22 porciento de ser de ET y un 6 porciento de ser 
de MZ. */

/* 2.4 En base al ejercicio anterior, crea un botón con el texto 'X' para cada uno 
de los p que hayas insertado y que si el usuario hace click en este botón 
eliminemos el parrafo asociado. */

const button$$ = document.querySelector('button');

const caculateProbability = (json, name$$) => {

    const countries = json.country;
    const probabilitiesByCountry = [];

    for (const country of countries) {
        console.log(country);
        const probability = Math.round(country.probability * 100);
        probabilitiesByCountry.push(`tiene un ${probability} porciento de ser de ${country.country_id}`)
    }
    return `El nombre ${name$$}\n${probabilitiesByCountry.join(',\n')}.`;
}

const result = (resultText) => {
    const result$$ = document.createElement('p');
    result$$.innerText = resultText;
    
    const button$$ = document.createElement('button');
    button$$.textContent = 'X';

    button$$.addEventListener('click', () => {
        result$$.remove();
        button$$.remove();
    });

    const div$$ = document.createElement('div');
    div$$.appendChild(result$$);
    div$$.appendChild(button$$);
    return div$$;
}

button$$.addEventListener('click', () => {
    
    const name$$ = document.querySelector('input').value;

    fetch(`${baseUrl}?name=${name$$}`)
    .then((response) => {
        return response.json();
    })
    .then((json) => {

        const resultText = caculateProbability(json, name$$);
        const result$$ = result(resultText);
        document.body.append(result$$);
    })
});



